﻿using Gtk;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;

namespace GithubAppGtk.Forms
{
    public class UserForm : Window
    {
        public UserForm(JObject user, Dictionary<JObject, JArray> reposCommits) : base("Center")
        {
            SetDefaultSize(600, 500);
            SetPosition(WindowPosition.Center);
            Title = "GithubAppGtk";

            ScrolledWindow scrolledWindow = new ScrolledWindow();

            Fixed fix = new Fixed();

            HttpClient httpClient = new HttpClient();
            Gdk.Pixbuf avatar = new Gdk.Pixbuf(httpClient.GetStreamAsync(user.GetValue("avatar_url").ToString()).Result, 200, 200);
            Image avatarImage = new Image(avatar);
            fix.Put(avatarImage, 200, 30);

            Label login = new Label(user.GetValue("login").ToString());
            fix.Put(login, 270, 250);

            Label createdAt = new Label("Created at " + user.GetValue("created_at").ToString());
            fix.Put(createdAt, 220, 270);

            Label reposTitle = new Label("Repositories:");
            fix.Put(reposTitle, 20, 290);

            int margin = 310;
            
            foreach (var repoCommits in reposCommits)
            {
                Label repo = new Label(
                    $"Name: {repoCommits.Key.GetValue("name")}" 
                    + $"\nCreated at: {repoCommits.Key.GetValue("created_at")}"
                    + $"\nUpdated at: {repoCommits.Key.GetValue("updated_at")}"
                    + $"\nPushed at: {repoCommits.Key.GetValue("pushed_at")}"
                    + $"\nLanguage: {repoCommits.Key.GetValue("language")}"
                    );

                repo.Text += "\nCommits:";

                foreach (JObject commit in repoCommits.Value)
                {
                    string message = (commit.GetValue("commit") as JObject).GetValue("message").ToString();
                    if (message.Contains("\n"))
                        message = message.Substring(0, message.IndexOf("\n"));
                    string date = ((commit.GetValue("commit") as JObject).GetValue("committer") as JObject).GetValue("date").ToString();
                    repo.Text += $"\n  {date}: {message}";
                }

                fix.Put(repo, 20, margin);

                margin += 105 + repoCommits.Value.Count * 15;
            }

            scrolledWindow.Add(fix);

            Add(scrolledWindow);

            ShowAll();
        }
    }
}
