﻿using System;
using Gtk;
using GithubAppGtk.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GithubAppGtk
{
    class Program : Window
    {
        private string userUri = "https://api.github.com/users/";
        public Program() : base("Center")
        {
            SetDefaultSize(450, 200);
            SetPosition(WindowPosition.Center);
            Title = "GithubAppGtk";
            TypeHint = Gdk.WindowTypeHint.Dialog;

            DeleteEvent += delegate { Application.Quit(); };

            Fixed fix = new Fixed();

            Entry entry = new Entry();
            fix.Put(entry, 140, 55);

            Label message = new Label(string.Empty);            
            fix.Put(message, 180, 140);

            Button btn1 = new Button("Get data");
            btn1.Clicked += async delegate 
            {
                if (!string.IsNullOrEmpty(entry.Text) && !string.IsNullOrWhiteSpace(entry.Text))
                {
                    message.Text = "Loading...";
                    string userName = entry.Text;

                    JObject user = (JObject)await GetData(userUri + userName);

                    if(user == null)
                        message.Text = "User not found";
                    else
                    {
                        var repos = await GetData(userUri + userName + "/repos") as JArray;
                        Dictionary<JObject, JArray> reposCommits = new Dictionary<JObject, JArray>();
                        
                        foreach (var repo in repos) {
                            string commitsUri = (repo as JObject).GetValue("commits_url").ToString().Replace("{/sha}", "");
                            var commits = await GetData(commitsUri) as JArray;
                            reposCommits.Add(repo as JObject, commits);
                        }

                        message.Text = string.Empty;
                        new UserForm(user, reposCommits);
                    }
                }
            };

            fix.Put(btn1, 180, 100);

            Label label = new Label("Enter a username");
            fix.Put(label, 175, 30);

            Add(fix);

            ShowAll();
        }

        public async Task<JToken> GetData(string uri)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(uri),
                Method = HttpMethod.Get,
            };
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.Headers.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
            request.Headers.Add("Authorization", "token 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");
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string result = await response.Content.ReadAsStringAsync();
                JToken data = JsonConvert.DeserializeObject<JToken>(result);
                return data;
            }

            return null;
        }

        static void Main(string[] args)
        {
            Application.Init();
            new Program();
            Application.Run();
        }
    }
}

//https://api.github.com/repos/MaxYakovlev/kursach_yii/commits
//https://api.github.com/users/MaxYakovlev/repos
//https://api.github.com/users/MaxYakovlev